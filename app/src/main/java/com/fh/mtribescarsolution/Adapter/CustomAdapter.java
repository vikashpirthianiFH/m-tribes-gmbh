package com.fh.mtribescarsolution.Adapter;

/**
 * Created by vikash on 03/11/17.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fh.mtribescarsolution.MainActivity;
import com.fh.mtribescarsolution.R;

import java.util.ArrayList;


public class CustomAdapter extends BaseAdapter {
    ArrayList<String> result;
    ArrayList<String> addressList;
    ArrayList<String> engineTypeList;
    ArrayList<String> exteriorList;
    ArrayList<String> interiorList;
    ArrayList<String> fuelList;
    ArrayList<String> vinList;

    Context context;
    private static LayoutInflater inflater = null;

    public CustomAdapter(MainActivity mainActivity, ArrayList<String> prgmNameList, ArrayList<String> prgmAddressList, ArrayList<String> prgmEngineTypeList,
                         ArrayList<String> prgmExteriorList, ArrayList<String> prgmInteriorList, ArrayList<String> prgmFuelList, ArrayList<String> prgmVinList) {
        // TODO Auto-generated constructor stub
        result = prgmNameList;
        addressList = prgmAddressList;
        engineTypeList = prgmEngineTypeList;
        exteriorList = prgmExteriorList;
        interiorList = prgmInteriorList;
        fuelList = prgmFuelList;
        vinList = prgmVinList;


        context = mainActivity;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return result.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder {
        TextView name;
        TextView address;
        TextView engineType;
        TextView exterior;
        TextView interior;
        TextView fuel;
        TextView vin;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder = new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.car_information, null);
        holder.name = (TextView) rowView.findViewById(R.id.name_id);
        holder.name.setText((CharSequence) result.get(position));


        holder.address = (TextView) rowView.findViewById(R.id.addres_id);
        holder.address.setText((CharSequence) addressList.get(position));


        holder.engineType = (TextView) rowView.findViewById(R.id.engineType_id);
        holder.engineType.setText((CharSequence) engineTypeList.get(position));

        holder.exterior = (TextView) rowView.findViewById(R.id.exterior_id);
        holder.exterior.setText((CharSequence) exteriorList.get(position));

        holder.interior = (TextView) rowView.findViewById(R.id.interior_id);
        holder.interior.setText((CharSequence) interiorList.get(position));

        holder.fuel = (TextView) rowView.findViewById(R.id.fuel_id);
        holder.fuel.setText((CharSequence) fuelList.get(position));

        holder.vin = (TextView) rowView.findViewById(R.id.vin_id);
        holder.vin.setText((CharSequence) vinList.get(position));

        rowView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Toast.makeText(context, "You Clicked " + result.get(position), Toast.LENGTH_SHORT).show();
            }
        });
        return rowView;
    }

}
