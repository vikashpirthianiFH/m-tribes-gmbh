package com.fh.mtribescarsolution;

import android.Manifest;
import android.content.Context;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;


import com.fh.mtribescarsolution.Adapter.CustomAdapter;
import com.fh.mtribescarsolution.Adapter.JsonParser;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by vikash on 03/11/17.
 */
public class MainActivity extends ActionBarActivity implements GoogleMap.OnMarkerClickListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private static final int MY_PERMISSION_REQUEST_READ_FINE_LOCATION = 100;
    private static final int MY_PERMISSION_REQUEST_INTERNET = 101;

    static final LatLng Hamburg = new LatLng(53.551086, 9.993682);
    private GoogleMap map;

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    ListView lv;
    Context context;


    JSONArray output;
    String name = "name";
    String address = "address";
    String engineType = "engineType";
    String exterior = "exterior";
    String interior = "interior";
    String fuel = "fuel";
    String vin = "vin";
    String coordinate = "coordinates";
    List<Marker> markers;
    int v = 0;
    ArrayList<Double> listLong;
    ArrayList<Double> listLat;
    AsyncTaskParseJson asyncTaskParseJson;
    boolean isConnected;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkPermission();


        context = this;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();


        if (isConnected == true) {
            asyncTaskParseJson = new AsyncTaskParseJson();


            // Here, thisActivity is the current activity

            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();


            mLocationRequest = LocationRequest.create()
                    .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                    .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                    .setFastestInterval(1 * 1000);


            try {
                output = asyncTaskParseJson.execute().get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }


            lv = (ListView) findViewById(R.id.expandableListView);

            TabHost tabHost = (TabHost) findViewById(R.id.tabHost);
            tabHost.setup();

            // List of all information  First Task
            TabHost.TabSpec tabSpec = tabHost.newTabSpec("list");
            tabSpec.setContent(R.id.tabList);
            tabSpec.setIndicator("List");
            tabHost.addTab(tabSpec);

            // Array list of latitude and longitude
            listLong = new ArrayList<Double>();
            listLat = new ArrayList<Double>();

            JSONArray strJson = new JSONArray();

            for (int i = 0; i < output.length(); i++) {

                try {
                    JSONObject c = output.getJSONObject(i);

                    strJson = c.getJSONArray(coordinate);

                    listLong.add(strJson.getDouble(0));
                    listLat.add(strJson.getDouble(1));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


            lv.setAdapter(new CustomAdapter(this, carInfoList(name), carInfoList(address), carInfoList(engineType), carInfoList(exterior), carInfoList(interior), carInfoList(fuel), carInfoList(vin)));

            //Create second "Map tab" to view Cars details  on a map Second Task
            tabSpec = tabHost.newTabSpec("map");
            tabSpec.setContent(R.id.tabMap);
            tabSpec.setIndicator("Map");
            tabHost.addTab(tabSpec);

            map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();

            markers = new ArrayList<Marker>();

            for (int i = 0; i < listLat.size(); i++) {

                Marker marker = map.addMarker(new MarkerOptions().position(new LatLng(listLat.get(i), listLong.get(i))));
                markers.add(marker);
                map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {

                        Log.d("marker", marker.getPosition().toString());
                        if (v % 2 == 0) {
                            hideMarker(marker);

                        } else {
                            marker.setTitle(null);
                            showMarker();
                        }
                        v++;
                        return false;
                    }
                });

            }

            markers.size();
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(Hamburg, 13));
            map.setMyLocationEnabled(true);


        } else {
            Toast.makeText(context, "Check Your internet connection  ", Toast.LENGTH_SHORT).show();

        }
    }


    public void hideMarker(Marker marker) {
        for (int i = 0; i < markers.size(); i++) {
            if (marker.getPosition().latitude == listLat.get(i) && marker.getPosition().longitude == listLong.get(i)) {
                marker.setVisible(true);
                marker.setTitle(carInfoList(name).get(i));
            } else {
                markers.get(i).setVisible(false);
            }
        }
    }


    public void showMarker() {
        for (int i = 0; i < markers.size(); i++) {
            markers.get(i).setVisible(true);
        }

    }

    private void handleNewLocation(Location location) {

        double currentLatitude = location.getLatitude();
        double currentLongitude = location.getLongitude();

        LatLng latLng = new LatLng(currentLatitude, currentLongitude);

    }

    public ArrayList<String> carInfoList(String data) {
        ArrayList<String> listdata = new ArrayList<String>();
        for (int i = 0; i < output.length(); i++) {

            try {
                JSONObject c = output.getJSONObject(i);
                String str = c.getString(data);
                listdata.add(str);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return listdata;
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (isConnected == true) {
            mGoogleApiClient.connect();
        } else {
            Toast.makeText(context, "Check Your internet  OnResume connection  ", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (isConnected == true) {

            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
                mGoogleApiClient.disconnect();
            }
        } else {
            Toast.makeText(context, "Check Your internet  connection  OnPause ", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSION_REQUEST_READ_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.


                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            case MY_PERMISSION_REQUEST_INTERNET: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.


                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;

            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void onConnected(Bundle bundle) {
        checkPermission();

        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        } else {
            handleNewLocation(location);
        }
    }

    public void checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED
                ) {//Can add more as per requirement

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.INTERNET, Manifest.permission.ACCESS_NETWORK_STATE},
                    123);
        }


    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                 * Thrown if Google Play services canceled the original
                 * PendingIntent
                 */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
            /*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */

        }

    }

    @Override
    public void onLocationChanged(Location location) {
        handleNewLocation(location);

    }


    // you can make this class as another java file so it will be separated from your main activity.
    public class AsyncTaskParseJson extends AsyncTask<String, String, JSONArray> {

        final String TAG = "AsyncTaskParseJson.java";
        // set your json string url here
        String yourJsonStringUrl = "http://data.m-tribes.com/locations.json";

        // contacts JSONArray
        JSONArray dataJsonArr = null;

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected JSONArray doInBackground(String... arg0) {

            try {

                // instantiate our json parser
                JsonParser jParser = new JsonParser();

                // get json string from url
                JSONObject json = jParser.getJSONFromUrl(yourJsonStringUrl);

                // get the array of users
                dataJsonArr = json.getJSONArray("placemarks");


            } catch (JSONException e) {
                e.printStackTrace();
            }

            return dataJsonArr;
        }

        @Override
        protected void onPostExecute(JSONArray strFromDoInBg) {


        }
    }

}

